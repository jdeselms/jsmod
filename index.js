import Vue from "https://unpkg.com/vue@2.6.11/dist/vue.esm.browser.min.js"
import { getNames } from './getNames.mjs'

const names = getNames()

new Vue({
    el: '#modtest',
    data: { 
        names: names.toString()
    }
})
