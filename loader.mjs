/*
 * Since Node doesn't have built-in http caching, this custom loader can do that job.
 */

import crypto from 'crypto'
import fs from 'fs'

// Create the folder if it doesn't exit.
if (!fs.existsSync('./__cache')) {
    await fs.promises.mkdir('./__cache')
}

// This hook loads modules, and if the module is an "https:" URL, then
// we'll try to read from a local cache. Note that this caching behavior
// is automatically handled when you resolve modules in the browser.
//
// The Node loader API is defined here.
// https://nodejs.org/api/esm.html#loadurl-context-nextload
export async function load(url, context, nextLoad) {
    // This is just a file, so just load it from the file system.
    // (calling nextLoad basically uses the default loader)
    if (url.startsWith('file://')) {
        return nextLoad(url)
    }

    // We're loading from the interwebs; try to read from the cache
    if (url.startsWith('https://')) {
        const text = await loadUrl(url)
        
        return {
            format: 'module',
            shortCircuit: true,
            source: text
        }
    }

    throw new Error("Don't know how to read url " + url)
}

// This caching strategy is very basic; it doesn't think about cache-control headers
// or anything. there are probably libraries out there to do this in a way that honors
// the cache headers
async function loadUrl(url) {
    const fromCache = await loadFromCache(url)
    if (fromCache) {
        return fromCache
    }

    const result = await fetch(url)
    const text = await result.text()

    await writeToCache(url, text)

    return text
}

async function loadFromCache(url) {
    const sha = getSha(url)

    try {
        return await fs.promises.readFile('./__cache/' + sha, 'utf-8')
    } catch (err) {
        return null
    }
}

async function writeToCache(url, text) {
    const sha = getSha(url)

    await fs.promises.writeFile('./__cache/' + sha, text, 'utf-8')
}

// I convert the URL into a SHA as a lazy way to ensure that we don't have any issues with long filenames or invalid characters.
export default function getSha(text) {
    return crypto.createHash('sha1').update(text).digest('hex')
}

