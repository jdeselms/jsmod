# Dynamic module resolution demo

Javascript in the browser has been able to load modules directly from an external URL for several years now. It's very common to load modules directly using "unpkg.com", as one example.

Dynamic module resolution isn't supported (yet) in node, though there are experimental flags you can
use to enable it.

If you supply the "--experimental-network-imports", then node will allow you to load modules directly
from an external URL. However, this does mean that whenever you start your program, you're going to have the overhead of reloading all the files, since Node doesn't have built-in caching of its fetch API.

There's another solution for that: Node allows you to hook into the module loader using the "--experimental-loader" flag. I've provided one here so that external modules will be stored to a local __cache folder. This saves times on repeated runs when running the Node version.

## Running locally

There are two versions of the demo, a Node version, and a browser version.

*Note* Since the modules are all installed dynamically, there is no reason to run `npm i`

### Running the node version
To run the node version: `npm start`.

The first time it runs, the packages will all be downloaded to your `__cache` folder. You can always delete this folder to force a reload.

### Running in a browser
* Run `npm run serve`
* Go to `http://localhost:3000` in your browser.

## Dynamic modules = using semantic versioning to automatically keep modules in sync

If you download your packages directly from "unpkg.com", then you can specify a semantic version
in your URL, such as: https://unpkg.com/yaml@1||2/dist/vue.esm.browser.min.js, then it will
automatically resolve to the latest version that satisfies your semantic version, meaning that as
your client libraries are updated, you don't need to do anything explicit to update the packages,
unless you need to update your semver because of a breaking change.

# Notes and observations

The Node implementation is pretty simplistic, since it doesn't obey "Cache-Control" headers. I would assume that there are already libraries out there that give you a local cache that obeys Cache-Control rules. (Our own @vp/http-cache could be adapted for this.)

However, the browser version takes full advantage of your browser's built in caching capabilities.
