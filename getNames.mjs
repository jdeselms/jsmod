import YAML from "https://unpkg.com/yaml@1||2/browser/index.js"
import sortBy from "https://unpkg.com/lodash-es@4.17.21/sortBy.js"
import map from "https://unpkg.com/lodash-es@4.17.21/map.js"

export function getNames() {

    const ytext = `
    people: 
        - name: Fred
        - name: Isabella
        - name: Bob
        - name: Maria
        - name: Leslie
    `

    const doc = YAML.parse(ytext)

    const names = sortBy(map(doc.people, p => p.name))

    return names
}